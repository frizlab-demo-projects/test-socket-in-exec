import Foundation

import CMacroExports



func send(fd: CInt, destfd: CInt, to socket: CInt) throws {
	var fd = fd /* A var because we use a pointer to it at some point, but never actually modified */
	let sizeOfFd = MemoryLayout.size(ofValue: fd) /* We’ll need this later */
	let sizeOfDestfd = MemoryLayout.size(ofValue: destfd) /* We’ll need this later */
	
	var msg = msghdr()
	
	/* We’ll place the destination fd (a simple CInt) in an iovec. */
	let iovBase = UnsafeMutablePointer<CInt>.allocate(capacity: 1)
	defer {iovBase.deallocate()}
	iovBase.initialize(to: destfd)
	
	let ioPtr = UnsafeMutablePointer<iovec>.allocate(capacity: 1)
	defer {ioPtr.deallocate()}
	ioPtr.initialize(to: iovec(iov_base: iovBase, iov_len: sizeOfDestfd))
	
	msg.msg_iov = ioPtr
	msg.msg_iovlen = 1
	
	/* Ancillary data. This is where we send the actual fd. */
	let buf = UnsafeMutablePointer<CInt>.allocate(capacity: 1)
	defer {buf.deallocate()}
	buf.initialize(to: -1)
	
	msg.msg_control = UnsafeMutableRawPointer(buf)
	msg.msg_controllen = socklen_t(XCT_CMSG_SPACE(sizeOfFd))
	
	guard let cmsg = XCT_CMSG_FIRSTHDR(&msg) else {
		throw Err("CMSG_FIRSTHDR returned nil.")
	}
	
	cmsg.pointee.cmsg_type = SCM_RIGHTS
	cmsg.pointee.cmsg_level = SOL_SOCKET
	
	cmsg.pointee.cmsg_len = socklen_t(XCT_CMSG_LEN(sizeOfFd))
	memmove(XCT_CMSG_DATA(cmsg), &fd, sizeOfFd)
	
	guard sendmsg(socket, &msg, /*flags: */0) != -1 else {
		throw Err(errno: errno)
	}
	logMsg("sent fd \(fd) through socket to child process")
}


func receiveFd(from socket: CInt) throws -> (receivedFd: CInt, expectedDestinationFd: CInt)? {
	var msg = msghdr()
	
	/* We receive the destination fd (a simple CInt) in an iovec. */
	let iovBase = UnsafeMutablePointer<CInt>.allocate(capacity: 1)
	defer {iovBase.deallocate()}
	iovBase.initialize(to: -1)
	
	let ioPtr = UnsafeMutablePointer<iovec>.allocate(capacity: 1)
	defer {ioPtr.deallocate()}
	ioPtr.initialize(to: iovec(iov_base: iovBase, iov_len: MemoryLayout<CInt>.size))
	
	msg.msg_iov = ioPtr
	msg.msg_iovlen = 1
	
	/* Ancillary data. This is where we receive the actual fd. */
	let controlBufSize = 256
	let controlBuf = UnsafeMutablePointer<Int8>.allocate(capacity: controlBufSize)
	defer {controlBuf.deallocate()}
	controlBuf.assign(repeating: 0, count: controlBufSize)
	msg.msg_control = UnsafeMutableRawPointer(controlBuf)
	msg.msg_controllen = socklen_t(controlBufSize)
	
	let receivedBytes = recvmsg(socket, &msg, 0)
	guard receivedBytes >= 0 else {
		/* The socket is not a TCP socket, so we cannot know whether the connexion
		 * was closed before reading it. We check error after reading and ignore
		 * connection reset error. */
		let ok = (receivedBytes == 0 || errno == ECONNRESET)
		if ok {return nil}
		else  {throw Err(errno: errno)}
	}
	
	let expectedDestinationFd = iovBase.pointee
	
	var receivedFd: Int32 = -1
	let cmsg = XCT_CMSG_FIRSTHDR(&msg)
	memmove(&receivedFd, XCT_CMSG_DATA(cmsg), MemoryLayout.size(ofValue: receivedFd))
	
	guard receivedFd != -1, expectedDestinationFd != -1 else {
		throw Err("internal error")
	}
	return (receivedFd: receivedFd, expectedDestinationFd: expectedDestinationFd)
}

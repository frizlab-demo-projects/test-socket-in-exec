import Foundation

import CMacroExports


let subProcess = CommandLine.argc > 1

if !subProcess {mainProcessMain()}
else           {subProcessMain()}


func mainProcessMain() {
	do    {try launchSubProcessAndWait(CommandLine.arguments[0])}
	catch {logMsg("error in main: \(error)")}
}

func subProcessMain() {
	do {
		logMsg("is valid descriptor \(isValidFileDescriptor(FileHandle.standardInput.fileDescriptor))")
		logMsg("is file descriptor \(isFileFileDescriptor(FileHandle.standardInput.fileDescriptor))")
		logMsg("is fifo descriptor \(isFifoFileDescriptor(FileHandle.standardInput.fileDescriptor))")
		logMsg("is socket descriptor \(isSocketFileDescriptor(FileHandle.standardInput.fileDescriptor))")
		logMsg("is char special descriptor \(isCharSpecialFileDescriptor(FileHandle.standardInput.fileDescriptor))")
		
		var destinationFdToReceivedFd = [CInt: CInt]()
		var receivedFdToDestinationFd = [CInt: CInt]()
		while let (receivedFd, destinationFd) = try receiveFd(from: FileHandle.standardInput.fileDescriptor) {
			receivedFdToDestinationFd[receivedFd] = destinationFd
			destinationFdToReceivedFd[destinationFd] = receivedFd
		}
		close(FileHandle.standardInput.fileDescriptor)
		
		for destinationFd in destinationFdToReceivedFd.keys {
			let receivedFd = destinationFdToReceivedFd[destinationFd]!
			defer {
				assert(receivedFdToDestinationFd[receivedFd] == destinationFd)
				receivedFdToDestinationFd.removeValue(forKey: receivedFd)
			}

			/* dup2 takes care of this case, but needed later. */
			guard destinationFd != receivedFd else {continue}

			guard dup2(receivedFd, destinationFd) != -1 else {
				throw Err("cannot dup2")
			}
			guard close(receivedFd) == 0 else {
				throw Err("cannot close")
			}
		}
		
		while let data = try FileHandle.standardInput.read(upToCount: 5) {
			logMsg("Received \(data.count) bytes")
		}
	} catch {
		logMsg("error in sub: \(error)")
	}
}

private func isValidFileDescriptor(_ fd: CInt) -> Bool {
	return fcntl(fd, F_GETFL) != -1 || errno != EBADF
}

private func isSocketFileDescriptor(_ fd: CInt) -> Bool {
	return issocket(fd) != 0
}

private func isCharSpecialFileDescriptor(_ fd: CInt) -> Bool {
	return ischarspecial(fd) != 0
}

private func isFifoFileDescriptor(_ fd: CInt) -> Bool {
	return isfifo(fd) != 0
}

private func isFileFileDescriptor(_ fd: CInt) -> Bool {
	return isfile(fd) != 0
}

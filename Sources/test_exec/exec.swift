import Foundation



func launchSubProcessAndWait(_ path: String) throws {
	var sv: (CInt, CInt) = (-1, -1)
	guard socketpair(/*domain: */AF_UNIX, /*type: */SOCK_DGRAM, /*protocol: */0, /*socket_vector: */&sv.0) == 0 else {
		/* TODO: Throw a more informative error? */
		throw Err(errno: errno)
	}
	
	let p = Process()
	p.executableURL = URL(fileURLWithPath: path)
	p.arguments = ["child"]
	p.standardInput = FileHandle(fileDescriptor: sv.1)
	let fdToSendFds = sv.0
	
	var fileDescriptorsToSend: [CInt /* Value in **child** */: CInt /* Value in **parent** */] = [:]
	fileDescriptorsToSend[FileHandle.standardInput.fileDescriptor] = FileHandle.standardInput.fileDescriptor
	
	logMsg("Launching sub")
	try p.run()
	
	Thread.sleep(forTimeInterval: 1)
	for (fdInChild, fdToSend) in fileDescriptorsToSend {
		try send(fd: fdToSend, destfd: fdInChild, to: fdToSendFds)
	}
	close(fdToSendFds)
	
	p.waitUntilExit()
}

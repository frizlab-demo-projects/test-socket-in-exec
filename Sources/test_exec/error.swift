import Foundation



struct Err : Error, CustomStringConvertible {
	
	var message: String
	
	init(_ message: String) {
		self.message = message
	}
	
	init(errno: CInt) {
		let cMessage = strerror(errno).flatMap{ String(cString: $0) } ?? "unknown"
		self.message = "system error: \(cMessage)"
	}
	
	var description: String {
		return message
	}
	
}

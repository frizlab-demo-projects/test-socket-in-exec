// swift-tools-version:5.4
import PackageDescription


let package = Package(
	name: "test_exec",
	platforms: [
		.macOS(.v11)
	],
	products: [
		.executable(name: "test_exec", targets: ["test_exec"])
	],
	targets: [
		.target(name: "CMacroExports"),
		.executableTarget(name: "test_exec", dependencies: ["CMacroExports"])
	]
)
